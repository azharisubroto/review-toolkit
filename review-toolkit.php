<?php
/**
 * @package zeytin_extensions
 * @version 1.0
 */
/*
Plugin Name: Review Toolkit
Plugin URI: http://juarathemes.com
Description: Review plugin
Author: Juarathemes
Version: 1.0.0
Author URI: http://juarathemes.com
*/

//plugin language file
define( 'ZL_REVIEWEXT_VER', '1.0.0' );
define( 'ZL_DIR', plugin_dir_path(__FILE__) ) ;
define( 'ZL_URL', plugin_dir_URL(__FILE__) );
define( 'ZL_EXT_URL', ZL_URL . '/extensions' );
define( 'ZL_REVIEWEXT_URL', ZL_EXT_URL . '/reviewtoolkit' );


//plugin language file
function zl_review_toolkit(){
    load_plugin_textdomain('reviewtoolkit', false, basename( dirname( __FILE__ ) ) . '/languages');
}
add_action('admin_head','zl_review_toolkit');

/**
 * @internal
 */
function zl_review_toolkit_extensions_locator($locations) {
    $locations[ dirname(__FILE__) . '/extensions' ]
    =
    plugin_dir_url( __FILE__ ) . 'extensions';

    return $locations;
}
add_filter('fw_extensions_locations', 'zl_review_toolkit_extensions_locator');

/*
{extension-name}/
├─manifest.php  # Data about extension: version, name, dependencies, etc.
├─class-fw-extension-{extension-name}.php # class FW_Extension_{Extension_Name} extends FW_Extension { ... }
├─config.php    # Extension specific configurations
├─static.php    # wp_enqueue_style() and wp_enqueue_script()
├─posts.php     # register_post_type() and register_taxonomy()
├─hooks.php     # add_filter() and add_action()
├─helpers.php   # Helper functions and classes
├─readme.md.php # Install instructions
├─options/
│ ├─posts/      # Post types options
│ │ ├─post.php
│ │ ├─{post-type}.php
│ │ └─...
│ ├─taxonomies/ # Taxonomies terms options
│ │ ├─category.php
│ │ ├─post_tag.php
│ │ ├─{taxonomy}.php
│ │ └─...
│ └-...
├─settings-options.php # Extension Settings page options
├─views/
│ └─...
├─static/
│ ├─js/
│ ├─css/
│ └─...
├─includes/ # All .php files are auto included (no need to require_once)
│ ├─other.php
│ └─...
└───[extensions/] # Directory for sub extensions */
?>
