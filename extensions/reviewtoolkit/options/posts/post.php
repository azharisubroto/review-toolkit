<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'rating' => array(
		'title'   => 'Review Options',
		'type'    => 'box',
		'options' => array(
			'reviewdecide'       => array(
				'type'         => 'multi-picker',
				'label'        => false,
				'desc'         => false,
				'picker'       => array(
					'reviewenable' => array(
						'label'        => __( 'Enable Review', 'reviewtoolkit' ),
						'desc'        => __( 'Switch to enable to add review box. The rest options will appear below', 'reviewtoolkit' ),
						'type'         => 'switch',
						'right-choice' => array(
							'value' => 'yes',
							'label' => __( 'Enable', 'reviewtoolkit' )
						),
						'left-choice'  => array(
							'value' => 'no',
							'label' => __( 'Disable', 'reviewtoolkit' )
						),
						'value'        => 'no',
					)
				),
				'choices'      => array(
					'yes' => array(
						'reviewposition' => array(
							'type'    => 'select',
							'label'   => esc_html__( 'Review Box Position', 'reviewtoolkit' ),
							'choices' => array(
								'top' 	=> esc_html__( 'Top', 'reviewtoolkit' ),
								'bottom' 	=> esc_html__( 'Bottom', 'reviewtoolkit' ),
							)
						),
						'reviewstyle' => array(
							'type'    => 'select',
							'label'   => esc_html__( 'Review Style', 'reviewtoolkit' ),
							'choices' => array(
								'stars' 	=> esc_html__( 'Stars', 'reviewtoolkit' ),
								'percent' 	=> esc_html__( 'Percent', 'reviewtoolkit' ),
								'points' 	=> esc_html__( 'Points', 'reviewtoolkit' ),
							)
						),
						'reviewheadline' => array(
							'type'    => 'text',
							'label'   => esc_html__( 'Review Headline', 'reviewtoolkit' ),
							'desc'    => esc_html__( 'A headline about the review (optional)', 'reviewtoolkit' ),
						),
						'averagelabel' => array(
							'type'    => 'text',
							'label'   => esc_html__( 'Average Score Label', 'reviewtoolkit' ),
							'desc'    => esc_html__( 'Such as: "EXCELLENT", "MASTERPIECE", etc. (optional)', 'reviewtoolkit' ),
						),
						'reviewsummary' => array(
							'type'    => 'wp-editor',
							'label'   => esc_html__( 'Review Summary', 'reviewtoolkit' ),
							'desc'    => esc_html__( 'A short conclusion about the review (optional)', 'reviewtoolkit' ),
							'reinit' => true,
							'size' => 'large',
							'editor_height' => 200,
						),

						// Additional CTA button
						/*
						'ctaenable'       => array(
							'type'         => 'multi-picker',
							'label'        => false,
							'desc'         => false,
							'picker'       => array(
								'ctaison' => array(
									'label'        => __( 'CTA Button', 'reviewtoolkit' ),
									'desc'        => __( 'Additional Call to action button that\'ll located under review summary', 'reviewtoolkit' ),
									'type'         => 'switch',
									'right-choice' => array(
										'value' => 'yes',
										'label' => __( 'Enable', 'reviewtoolkit' )
									),
									'left-choice'  => array(
										'value' => 'no',
										'label' => __( 'Disable', 'reviewtoolkit' )
									),
									'value'        => 'no',
								)
							),
							'choices'      => array(
								'yes' => array(
									'ctaurl'  => array(
										'type'  => 'text',
										'label' => __( 'Target URL', 'reviewtoolkit' ),
									),
									'ctatext' => array(
										'type'  => 'text',
										'label' => __( 'Button Label', 'reviewtoolkit' ),
									)
								),
							),
							'show_borders' => false,
						),
						*/
						
						// Review CRITERIA
						'reviewcriterias'  => array(
							'label'        => __( 'Review Criteria', 'reviewtoolkit' ),
							'type'         => 'addable-box',
							'value'        => array(),
							'box-controls' => array(//'custom' => '<small class="dashicons dashicons-smiley" title="Custom"></small>',
							),
							'box-options'  => array(
								'criterialabel'     => array(
									'label' => __( 'Label', 'reviewtoolkit' ),
									'type'  => 'text',
								),
								'criteriascore' => array(
									'label' => __( 'Slider', 'reviewtoolkit' ),
									'type'  => 'slider',
									'value' => 50,
									'properties' => array(
								        'min' => 0,
								        'max' => 100,
								        'step' => 1, // Set slider step. Always > 0. Could be fractional.
									)
								),
							),
							'template'     => '{{- criterialabel }}: {{- criteriascore }}',
						),
						'pros'  => array(
							'type'  => 'addable-option',
						    'label' => __('Pros', 'reviewtoolkit'),
						    'desc'  => __( 'The Good Things of the review article', 'reviewtoolkit' ),
						    'option' => array( 'type' => 'text' ),
						    'add-button-text' => __('Add', 'reviewtoolkit'),
						    'sortable' => true,
						),
						'cons'  => array(
							'type'  => 'addable-option',
						    'label' => __('Cons', 'reviewtoolkit'),
						    'desc'  => __( 'The Bad Things of the review article', 'reviewtoolkit' ),
						    'option' => array( 'type' => 'text' ),
						    'add-button-text' => __('Add', 'reviewtoolkit'),
						    'sortable' => true,
						),
					),
				),
				'show_borders' => true,
			),

		),
	),

);
