<?php
if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Extension_Reviewtoolkit extends FW_Extension {
	/**
	* @internal
	*/
	public function _init() {
		if ( is_admin() ) {
			$this->add_admin_filters();
		}
		$this->public_filters();
	}

	public function add_admin_filters() {
		add_filter( 'fw_post_options', array( $this, '_filter_admin_add_post_options' ), 10, 2 );
	}

	private function public_filters() {
		add_action('the_content', array( $this, '_filter_review_markup' ), 10, 2);
		add_action( 'wp_enqueue_scripts', array( $this, '_frontend_action_add_static' ), 10, 2 );
	}

	public function _frontend_action_add_static(){
		global $post;
		$css = ZL_REVIEWEXT_URL . '/static/css/reviewtoolkit.css';
		if( is_singular('post') ){
			wp_enqueue_style( 'zl-reviewtoolkit', $css, array(), ZL_REVIEWEXT_VER );
		}
	}

	/**
	* @internal
	*
	* @param array $options
	* @param string $post_type
	*
	* @return array
	*/
	public function _filter_admin_add_post_options( $options, $post_type ) {
		if ( $post_type === 'post' ) {
			$options[] = array(
				$this->get_post_options('post')
			);
		}

		return $options;
	}

	/**
	* @internal
	*
	* @param array $options
	* @param string $post_type
	*
	* @return array
	*/
	public function _filter_review_markup( $content ) {
		global $post;

		// if not a blog post, return original $content
		if( !is_singular('post') ) return $content;

		// Get the review meta post
		$reviewdecide = fw_get_db_post_option( $post->ID, 'reviewdecide' );

		// If not yes, return only $content;
		if( 'yes' != $reviewdecide['reviewenable'] ) return $content;

		$reviewposition = 'bottom';

		if( is_array( $reviewdecide ) && !empty( $reviewdecide ) ){
			if( 'yes' == $reviewdecide['reviewenable'] ){
				$average = $criterias = $reviewstyle = $points = $criteriaitems = $sum = $average = '';

				$criterias = $reviewdecide['yes']['reviewcriterias'];
				$reviewstyle = $reviewdecide['yes']['reviewstyle'];

				// Get the average Point
				$points = array();
				if( is_array( $criterias ) ){
					foreach( $criterias as $criteria ){
						$points[] = $criteria['criteriascore'];
					}
				}
				$criteriaitems = count( $criterias );
				$sum = array_sum( $points );
				$average = $sum / $criteriaitems;

				// Display the score in different way
				if( 'points' == $reviewstyle ){
					// Points
					$average = number_format($average/10, 1, '.', ''); // X.X
				} elseif( 'stars' == $reviewstyle ){
					// Stars
					$average = number_format($average/10, 1, '.', ''); // X.X
				} else {
					// Percent
					$average = number_format($average, 1, '.', '').'<span>%</span>'; // XX.X%
				}

				// Review Box Markup
				$review = $reviewposition = $reviewstyle = $reviewheadline = $reviewheadline = $averagelabel = $reviewsummary = '';

				$review = $reviewdecide['yes'];
				$reviewstyle = $review['reviewstyle'];
				$position = $review['reviewposition'];
				$reviewheadline = $review['reviewheadline'];
				$averagelabel = $review['averagelabel'];
				$reviewsummary = $review['reviewsummary'];
				$pros = $review['pros'];
				$cons = $review['cons'];
				ob_start();
				?>

				<div id="zl_review_box">

					<!-- REVIEW HEAD -->
					<div class="zl_review_head">
						<h2><?php echo esc_html( $reviewheadline ); ?></h2>
					</div>

					<?php
					if( $criterias AND is_array( $criterias ) ):
						?>
						<!-- REVIEW CRITERIAS -->
						<div class="zl_review_criterias">
							<?php
							foreach( $criterias as $criteria ){
								?>
								<!-- Criteria Item -->
								<div class="zl_review_criteria_item">

									<!-- Label -->
									<div class="zl_review_criteria_item_label">
										<?php echo esc_html( $criteria['criterialabel'] ); ?>
									</div> <!-- .zl_review_criteria_item_label -->

									<!-- Score -->
									<div class="zl_review_criteria_item_score">
										<?php
										if( 'percent' == $reviewstyle ){
											echo esc_html( $criteria['criteriascore'] ).'%';
										} elseif( 'points' == $reviewstyle ) {
											echo esc_html( $criteria['criteriascore']/10 );
										} else {
											$starsmarkup = '<span class="star-rating"><span style="width:'. esc_attr( $criteria['criteriascore'] ).'%"></span></span>';

											echo wp_kses( $starsmarkup, array( 'span' => array( 'class' => array(), 'style' => array() ) ) );
										}
										?>
									</div> <!-- .zl_review_criteria_item_score -->
									<div class="clear"></div>

									<?php
										if( 'stars' != $reviewstyle ){
									?>
										<!-- Score Bar -->
										<div class="zl_review_indicator_base">
											<?php
											$width = $criteria['criteriascore'];
											?>
											<div class="zl_review_indicator_actual" style="width: <?php echo esc_attr( $width ); ?>%;"></div>
										</div> <!-- .zl_review_indicator_base -->
									<?php
										}
									?>
									<div class="clear"></div>
								</div>
								<?php
							}
							?>
						</div>
						<?php
					endif;
					?>

					<!-- POSTIVES AND NEGATIVES -->
					<div class="posconwrapper">
						<?php
						if( $pros ){
							echo '<ul class="procons">';
							foreach( $pros as $pro ){
								echo '<li>';
								echo '<span class="fa fa-plus"></span><span class="zl_alt_icon">+</span> '.$pro;
								echo '</li>';
							}
							echo '</ul>';
						}
						if( $cons ){
							echo '<ul class="procons negative">';
							foreach( $cons as $con ){
								echo '<li>';
								echo '<span class="fa fa-minus"></span><span class="zl_alt_icon">-</span> '.$con;
								echo '</li>';
							}
							echo '</ul>';
						}
						?>
					</div>


					<!-- REVIEW FOOTER -->
					<div class="zl_review_footer">

						<!-- Summary -->
						<div class="zl_review_summary">
							<?php echo wpautop( $reviewsummary ); ?>
						</div>

						<!-- AVERAGE FINAL -->
						<div class="zl_review_avg_box">
							<div class="zl_averagescore">
								<?php
									// Display the score in different way
									if( 'points' == $reviewstyle ){
										// Points
								?>
										<div class="rich-snippet-text" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
											<meta itemprop="worstRating" content="0">
											<div itemprop="ratingValue">
												<?php echo wp_kses( $average , array( 'span' => array() ) ); ?>
											</div>
											<meta itemprop="bestRating" content="10">
										</div>
								<?php
									} elseif( 'stars' == $reviewstyle ){
										// Stars
								?>
										<div class="rich-snippet-text" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
											<meta itemprop="worstRating" content="0">
											<meta itemprop="ratingValue" content="<?php echo esc_attr( $average/2 ); ?>">
											<?php
												$avgstars = '<span class="star-rating"><span style="width:'. esc_attr( $average*10 ).'%"></span></span>';

												echo wp_kses( $avgstars, array( 'span' => array( 'class' => array(), 'style' => array() ) ) );
											?>
											<meta itemprop="bestRating" content="5">
										</div>
								<?php
									} else {
										// Percent
								?>
										<div class="rich-snippet-text" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
											<meta itemprop="worstRating" content="0">
											<div itemprop="ratingValue">
												<?php echo wp_kses( $average , array( 'span' => array() ) ); ?>
											</div>
											<meta itemprop="bestRating" content="100">
										</div>
   								<?php
									}
								 ?>
							</div>
							<div class="zl_averagelabel">
								<?php echo esc_html( $averagelabel ); ?>
							</div>
						</div>
					</div>
				</div>

				<?php
				$print = ob_get_clean();
				$aftercontent = $print;

				if( 'bottom' == $position ){
					$fullcontent = $content . $aftercontent;
				} elseif( 'top' == $position ) {
					$fullcontent = $aftercontent . $content;
				} else {
					$fullcontent = $content;
				}

				return $fullcontent;
			}
		}
	}
}

?>
